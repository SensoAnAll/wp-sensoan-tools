'''
This is the script to send data to destination from sinks

Created on 15.02.2017

@author: peic
'''

import argparse
import binascii
import sys
from wirepas.meshapi import *

# Change the data into the format, which can be used in serial port
def _convert_date_to_binary(data):
    if data == "":
        data = None

    if data != None:
        if data[:2] == '0x':
           data = data[2:]
        data = binascii.unhexlify(data)
    return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--destination',
                        default=255,
                        help='the destination endpoint')
    parser.add_argument('-s', '--source',
                        default=None,
                        help='the source endpoint')
    parser.add_argument('-p', '--packet',
                        help="the package to be sent. example:-p 0x0102030405060708090a0b0c0d0e0f")
    parser.add_argument('-a', '--address',
                        default=ADDRESS_BROADCAST,
                        help="the destination address")

    args = parser.parse_args()

    # Check the arguments are valid
    if args.packet is None:
        print "Please add valid packet"
        sys.exit(1)

    if args.source is None:
        print "The source endpoint cannot be empty"
        sys.exit(1)

    if int(args.source)>255 or int(args.destination)>255:
        print "The end point should not be between 0 and 255"
        sys.exit(1)

    # Find the sinks
    network = MeshApiNetwork()
    network.find_devices()
    sinks = network.get_sinks()

    if len(sinks)==0:
        print "Please connect to at least one sink"
        sys.exit(1)
    else:
        print "The sinks connected are", str(sinks)

    # Check the packet's length
    packet_send = _convert_date_to_binary(args.packet)
    mtu = 0
    for sink in sinks:
        mtu = sink.get_mtu()['mtu']
        packet_length =len(packet_send)
        if packet_length>mtu:
            print "The data length should not exceed the sink's mtu length limit", str(mtu)
            sys.exit(1)

    print "The data",str(binascii.hexlify(packet_send)), "is going to be sent to", str(args.address)

    for sink in sinks:
        sink.data_tx(data=packet_send,
                     dest_address= int(args.address),
                     src_endpoint=int(args.source),
                     dst_endpoint=int(args.destination))

