from wirepas.meshapi import *
from wirepas.meshapicsap import *
from Queue import Empty
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inifile', default=None, help=".ini file for the Python API")

    args = parser.parse_args()
    if args.inifile != None:
        print "Loading .ini-file: "+str(args.inifile)
        config.load_file(args.inifile)

    # Create a network instance.
    network = MeshApiNetwork()

    # Find MeshApi devices connected to the computer.
    # config.py has the configuration for device and interface types that are used.

    print("Finding devices..")
    network.find_devices()

    print("Found: " + str(network.get_devices()))

    # Get the sink.
    sinks = network.get_sinks()

    for sink in sinks:
        try:
            sink.stack_start(autostart=True)
        except:
            pass

    for sink in sinks:
        print("Sink addr:" + str(sink.get_address()))
        print("Net addr:" + str(sink.get_network_address()))

        sink.start_node_diag_rx()
        sink.start_data_rx()


    nodes = {}
    while True:

        for sink in sinks:
            # Flush buffer
            
            try:
                while True:
                    data = sink.get_data_rx(timeout = 1)

            except Empty:
                sink.stop_data_rx()

            try:
                data = sink.get_node_diag_rx(timeout = 1)
            except Empty:
                continue

            source = data['source_address']
            if source not in nodes:
                nodes[source] = data
                
                nb_sinks = 0
                nb_headnodes = 0
                nb_subnodes = 0
                nb_autorole = 0

                for key in nodes:
                    role = nodes[key]['role']
                    if role == 4:
        	            nb_sinks = nb_sinks + 1
                    elif role == 2:
                        nb_headnodes = nb_headnodes + 1
                    elif role == 1:
                        nb_subnodes = nb_subnodes + 1
                    else:
                        nb_autorole = nb_autorole + 1

                print('>> Total amount of nodes {0}'.format(len(nodes), nb_sinks, nb_headnodes, nb_subnodes, nb_autorole))
                print('>> Total per type (sink - head - sub / auto ) {1}:{2}:{3}/{4}'.format(len(nodes), nb_sinks, nb_headnodes, nb_subnodes, nb_autorole))
                print('>> Latest {0}'.format(source))
              
                print('>> Headnode:')
                counter = 0
                for key in sorted(nodes):
                    if nodes[key]['role'] == 2:
                        counter = counter + 1
                        print('{{"name": A{2:0>3}, "nid": {0}, "role": {1}}},'.format(key, nodes[key]['role'], counter ))

                print('>> Subnode:')
                counter = 0
                for key in sorted(nodes):
                    if nodes[key]['role'] == 1:
                        counter = counter + 1
                        print('{{"name": M{2:0>3}, "nid": {0}, "role": {1}}},'.format(key, nodes[key]['role'], counter ))

                print('>> Autorole:')
                counter = 0
                for key in sorted(nodes):
                    if nodes[key]['role'] == 130:
                        counter = counter + 1
                        print('{{"name": R{2:0>3}, "nid": {0}, "role": {1}}},'.format(key, nodes[key]['role'], counter ))

    sink.stop_node_diag_rx()
    network.kill()
