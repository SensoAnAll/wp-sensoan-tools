
# Copyright 2017 Sensoan OY. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This is an example of enable beacon and send an ibeacon from sink.
Note: Always better to use eddystone or ibeacon, since there are scan applications for phone

It works as following:
. compose the data package(mode + interval + beacon payload)
. send the data package (endpoint 255->242) to the target node so the node will send the beacon

To customize the beacon, the interval, uuid, version, power can be changed

'''

#import random
from wirepas.beacon.beacondata_command import *
from wirepas.meshapi_beacon_functions import *
from wirepas.meshapi import *

import sys
import argparse
from wirepas.configuration import config



def nodeset_sorted_str(nodeset):
    sorted = list(nodeset)
    sorted.sort()
    s = ', '.join(map(str, sorted))
    return s

def validate_hexdecimal_payload(length, payloadString):
    if len(payloadString)== 2*length:
        for c in payloadString:
            if ((c >='0') and (c<='9')) \
               or ((c >='a') and (c<='f')) \
               or ((c >='A') and (c<='F')):
               pass
            else:
                print "The string {} is not hexdecimal, 0x is not needed!!".format(payloadString)
                return False
        return True
    else:
        print "The string length, {}, is not as specified 2*{}.".format(len(payloadString), length)
        return False



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nodelist', default=None,
                        help="A comma separated node list from command line")
    parser.add_argument('-i', '--inifile', default=None, help=".ini file for the Python API")
    parser.add_argument('-t', '--interval', default="100",
                        help="iBeacon broadcasting interval in ms. Default, 100ms")
    parser.add_argument('-u', '--payloadUUID', default=None,
                        help="iBeacon payload UUID part, 16 hexdecimal without 0X. Default, None")
    '''
    Note that the Major and Minor in wp using little endians, so that the 
    numbers are different than asset tracking firmware, i.e 5849535 => 59 41BF
    asset FW:      major = 0x0059 = 89,    minor = 0x41BF = 16831
    WP reomte API: major = 0x5900 = 22784, minor = 0xBF41 = 48961
    '''

    args = parser.parse_args()

    nodeset = set()
    interval = 100
    uuid_in_hex_decimal = None


    if args.nodelist != None:
        nodeset.update(set(map(int, args.nodelist.split(','))))
    if len(nodeset) != 0:
        print "User inputed nodes: "+nodeset_sorted_str(nodeset)
    else:
        print("Node address is not input.")
        exit(0)

    if args.inifile != None:
        print "Loading .ini-file: "+str(args.inifile)
        config.load_file(args.inifile)

    if args.interval != None:
        interval = int(args.interval)
    print "iBeacon interval:  " + str(interval)
    
    if args.payloadUUID != None:
        uuid_in_hex_decimal = args.payloadUUID
        if not validate_hexdecimal_payload(16, uuid_in_hex_decimal):
            print("Payload validation failed.")
            exit(0)
        print "iBeacon uuid: {}".format(uuid_in_hex_decimal)
    else:
        print("Payload UUID must be added as 16 byte hexdecimal.")
        exit(0)




    beacon = BeaconCommandMessage()

    # set the mode as 1 to enable the sending
    moderequest1 = beacon.generate_mode_command(value=ModeValue.TX, value_format='uint8')

    # Set the beacon send interval, unit is ms, the range is 100-60000
    tx_interval = beacon.generate_frame_tx_interval(value=interval, value_format='uint16')

    address = list(nodeset)[0]

    uuid_string = binascii.unhexlify(uuid_in_hex_decimal)

    request_data={}

    for node in nodeset:
        print "uuid in string is {}".format(uuid_string)
        #print "uuid in hex decimal format is {}".format(uuid_in_hex_decimal)

        beacon_generated = beacon.create_ibeacon(index=0,                # The payload index, range is 0-7
                                                 device_address=node, # The device address
                                                 uuid=uuid_string,       # The uuid, if not set it will come from address(for demo)
                                                 major=None,             # The major version, if not set it will come from address(for demo)
                                                 minor=None,             # The minor version, if not set it will come from address(for demo)
                                                 measured_power=4)       # The power

        beacon_request = beacon.generate_payload(beacon_generated, value_format='string')
        print "{}".format(beacon_request)
        print type(beacon_request)
        request_data[node] = beacon_request

    print request_data

    
    for node in request_data:
        print request_data[node]

    # Find the sinks
    network = MeshApiNetwork()
    network.find_devices()
    sinks = network.get_sinks()

    # Be sure there is at least one sink
    if len(sinks) == 0:
        print "Warning** Needs at least one sink!"
        exit(0)
    else:
        for sink in sinks:
            retval = sink.get_stack_state()
            if retval != 0:
                try:
                    print "Info** Sink {} is off, will start the stack".\
                        format(sink.get_address())
                    sink.stack_start(autostart=True)
                except Exception as ex:
                    print 'Warning** Sink {} stack is off and cannot be started,ex:{}'.\
                        format(sink.get_address(), ex)

            print "Info**Send the command to the target device(here used sink) now"
            for node in request_data:
                print request_data[node]
                sink.beacon_request_tx(data= moderequest1+tx_interval+request_data[node],
                                   #dest_address=sink.get_address(),
                                   dest_address=node,
                                   src_endpoint=BEACON_ENDPOINT_REQUEST_SRC,
                                   dst_endpoint=BEACON_ENDPOINT_REQUEST_DST,
                                   timeout=60)


