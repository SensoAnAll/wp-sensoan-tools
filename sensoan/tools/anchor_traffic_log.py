from wirepas.meshapi import *
from wirepas.meshapicsap import *
from Queue import Empty
import time
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inifile', default=None, help=".ini file for the Python API")
    args = parser.parse_args()

    if args.inifile != None:
        print "Loading .ini-file: "+str(args.inifile)
        config.load_file(args.inifile)

    # Create a network instance.
    network = MeshApiNetwork()

    # Find MeshApi devices connected to the computer.
    # config.py has the configuration for device and interface types that are used.

    print("Finding devices..")
    network.find_devices()

    print("Found: " + str(network.get_devices()))

    # Get the sink.
    sinks = network.get_sinks()

    for sink in sinks:
        try:
            sink.stack_start(autostart=True)
        except:
            pass

    for sink in sinks:
        print("Sink addr:" + str(sink.get_address()))
        print("Net addr:" + str(sink.get_network_address()))

        #sink.start_node_diag_rx()
        sink.start_traffic_diag_rx()
        sink.start_data_rx()


    nodes = {}
    while True:

        for sink in sinks:
            # Flush buffer
            
            try:
                while True:
                    data = sink.get_data_rx(timeout = 1)

            except Empty:
                sink.stop_data_rx()

            try:
                #data = sink.get_node_diag_rx(timeout = 1)
                data = sink.get_traffic_diag_rx(timeout = 1)
                #print "traffic data:"
                #print data
            except Empty:
                continue

            source = data['source_address']

            if source not in nodes:
                nodes[source] = {'data': {}, 'tx_amount_start':'', 'tx_time_start':'', 'rx_amount_start':'', 'rx_time_start':''}
                nodes[source]['data']            = data
                nodes[source]['tx_amount_start'] = data['tx_amount']
                nodes[source]['tx_time_start']   = data['tx_unixtime']
                nodes[source]['rx_amount_start'] = data['rx_amount']
                nodes[source]['rx_time_start']   = data['tx_unixtime']
            else:
                nodes[source]['data']            = data

            print ('------------------------------------------------------------------------------------------------')
            for key in nodes:
                if key == 1:
                    print ('|Sink        :{:<10d}| rx Packet amount:{:<6d}| tx Packet amount:{:<6d}| in {:7.2f} minutes|'.format(key, \
                    int(nodes[key]['data']['rx_amount']) - int(nodes[key]['rx_amount_start']), \
                    int(nodes[key]['data']['tx_amount']) - int(nodes[key]['tx_amount_start']), \
                    (time.time() - min(float (nodes[key]['tx_time_start']), float (nodes[key]['tx_time_start'])))/60))
                else:
                    print ('|Anchor nodes:{:<10d}| rx Packet amount:{:<6d}| tx Packet amount:{:<6d}| in {:7.2f} minutes|'.format(key, \
                        int(nodes[key]['data']['rx_amount']) - int(nodes[key]['rx_amount_start']), \
                        int(nodes[key]['data']['tx_amount']) - int(nodes[key]['tx_amount_start']), \
                        (time.time() - min(float (nodes[key]['tx_time_start']), float (nodes[key]['tx_time_start'])))/60))

    #sink.stop_node_diag_rx()
    network.kill()
