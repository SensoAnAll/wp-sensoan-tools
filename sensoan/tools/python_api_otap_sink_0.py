'''
Copyright 2017 Sensoan. All Rights Reserved.

Safe OTAP script to do OTAP according Wirepas instructions.

Created on 2.2.2017
Status check and print added .9.17.2017

'''

from wirepas.meshapi import *
from wirepas.configuration import config
from wirepas.otapimage import *

import argparse
import time
from Queue import Empty
import binascii
import sys

def load_csv_nodelist(filename):
    f = open(filename)

    nodeset = set()
    
    for line in f:
        if line == '\n':
            continue
        nodeset.add(int(line))

    return nodeset

def nodeset_sorted_str(nodeset):
    sorted = list(nodeset)
    sorted.sort()
    s = ', '.join(map(str, sorted))
    return s

def print_status_information(status_dict):
    #print str(status_dict)
    print "----------------------------------------------------"
    print "Scratchpad length: "+str(status_dict['scratchpad_length'][0])
    print "Scratchpad CRC: "+hex(status_dict['crc'][0])
    print "Scratchpad sequence number: "+str(status_dict['otap_seq'][0])
    print "Scratchpad type: "+str(status_dict['scratchpad_type'][0])
    print "Scratchpad status: "+str(status_dict['scratchpad_status'][0])
    print "Processed scratchpad length: "+str(status_dict['processed_length'][0])
    print "Processed CRC: "+hex(status_dict['processed_crc'][0])
    print "Processed sequence number: "+str(status_dict['processed_seq'][0])
    print "FW memory area ID: "+str(status_dict['fw_mem_area_id'][0])
    print "FW major version: "+str(status_dict['fw_major_version'][0])
    print "FW minor version: "+str(status_dict['fw_minor_version'][0])
    print "FW maintenance version: "+str(status_dict['fw_maintenance_version'][0])
    print "FW development version: "+str(status_dict['fw_development_version'][0])

def progress_cb(percentage):
    if percentage == 0:
        return
    
    if percentage % 10 == 0:
        print str(percentage)+'%'
    else:
        print ".",
        sys.stdout.flush()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--scratchpad', default=None, 
                        help="The scratchpad file, and force scratchpad sequence number as 0.")
    parser.add_argument('-i', '--inifile', default=None, help=".ini file for the Python API")

    args = parser.parse_args()

    
    nodeset = set()
        
    if len(nodeset) != 0:
        print "User inputed nodes: "+nodeset_sorted_str(nodeset)
    if args.inifile != None:
        print "Loading .ini-file: "+str(args.inifile)
        config.load_file(args.inifile)

    print "Searching for devices.."
    network = MeshApiNetwork()
    network.find_devices()

    devices = network.get_devices()

    if len(devices) == 0:
        print "No devices found"
        exit(1)
        
    print "Found devices: "+str(devices)

    sinks = network.get_sinks()
    
    if len(sinks) == 0:
        print "At least one sink required"
        exit(1)

    sink_addresses = map(lambda x: x.get_address(), sinks)

    print "Sinks: "+", ".join(map(str, sink_addresses))

    print "Confirming that the sink stacks are started.."
    for sink in sinks:
        sink.stack_start(autostart=True)

        
    print "Get scratchpad status from sinks: "
    for sink in sinks:
        print "Sink: "+str(sink.get_address())
        image_status = sink.get_otap_image_status()
        print_status_information(image_status)
        #scratchpad_sequences.add(image_status['otap_seq'][0])
        #processed_sequences.add(image_status['processed_seq'][0])
        print "---------------------------------"

    if args.scratchpad == None:
        print "Not done, No scratchpad image defined!"
        exit(1)


    otap_sequence = 0
    #print "The nodes to be updated are: "+", ".join(map(str, otapped))
    
    print "---------------------------------------------"
    print "Network in a stable state. Starting OTAP."
    print "Loading the OTAP file.."
    
    image = OTAPImage()
    image.load_file(args.scratchpad)
    print "File tag: "+binascii.hexlify(image.file_tag)
    print "Image length: "+str(image.length)
    print "CRC: "+hex(image.crc)
    print "Sequence number: "+str(image.sequence_number)
    print "Pad: "+str(image.pad)
    print "Type: "+str(image.image_type)
    print "Image start: "+binascii.hexlify(image.image[:32])
    
    print "Uploading the image to sinks"    
    for sink in sinks:
        print "Sink "+str(sink.get_address())
        print "Start listening for OTAP status messages"
    
        print "Stop stack.."
        sink.stack_stop()

        print "Upload scratchpad image.."
        result = sink.load_otap_image(image, otap_sequence = otap_sequence,
                                        progress_callback = progress_cb, progress_step=1)
        if result == MsapScratchpadBlockResult.SuccessAllData:
            print "Image loaded succesfully!"
        else:
            print "ERROR: Image loading failed!"
            exit(1)

        print "Start stack.."
        sink.stack_start(autostart=True)

    print "Otap/scratchpad is loaded to Sink and sequence number is Zero."
    print "Done!!"
    